const { expect } = require("chai");

describe("MyNFT contract", () => {
    let Token;
    let hardhatToken;
    let owner;
    let addr1;
    let addr2;
    let addrs;

    beforeEach(async () => {
        Token = await ethers.getContractFactory("MyNFT");
        [owner, addr1, addr2, ...addrs] = await ethers.getSigners();

        hardhatToken = await Token.deploy();
    });

    describe("Deployment", () => {
        it("Should set the right owner", async () => {
            expect(await hardhatToken.owner()).to.equal(owner.address);
        });

        it("Should assign the total supply of tokens to the owner", async () => {
            const ownerBalance = await hardhatToken.balanceOf(owner.address);
            expect(await hardhatToken.totalSupply()).to.equal(ownerBalance);
        });
    });

    describe("Mint", () => {
        it("Should mint 3 tokens", async () => {
            await hardhatToken.mint(3);
            expect(await hardhatToken.totalMinted()).to.equal(3);
            expect(await hardhatToken.numberMinted(owner.address)).to.equal(3);

        });

        it("Test baseUri", async () => {
            const uri = 'https://test.so';
            await hardhatToken.setBaseURI(uri);
            expect(await hardhatToken.getBaseURI()).to.equal(uri);
        });

        it("Token URI", async () => {
            const uri = 'https://test.so';
            await hardhatToken.mint(1);
            await hardhatToken.setBaseURI(uri);
            expect(await hardhatToken.tokenURI(0)).to.equal('https://test.so/0.json');
        });

        it("Single mint", async () => {
            await hardhatToken.mintNFT('https://1');
            await hardhatToken.mintNFT('https://2');
            await hardhatToken.mintNFT('https://3');
            expect(await hardhatToken.getUri(2)).to.equal('https://2');
        });
    });
});