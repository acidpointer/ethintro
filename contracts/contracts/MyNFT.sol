// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "erc721a/contracts/ERC721A.sol";

import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Strings.sol";


contract MyNFT is ERC721A, Ownable {
    using Counters for Counters.Counter;

    Counters.Counter private tokenCounter;

    string private baseURI = "none";

    mapping(uint256 => string) urls;

    constructor() ERC721A("MyNFT", "NFT") {}

    function mintNFT(string memory url) external payable {
        _safeMint(msg.sender, 1);
        urls[nextTokenId()] = url;
    }

    function getUri(uint256 tokenId) external view returns (string memory) {
        return urls[tokenId];
    } 

    function mint(uint256 quantity) external payable {
        _safeMint(msg.sender, quantity);
    }

    function tokenURI(uint256 tokenId) public view virtual override returns (string memory) {
        require(_exists(tokenId), "Nonexistent token");
        return string(abi.encodePacked(baseURI, "/", Strings.toString(tokenId), ".json"));
    }

    function nextTokenId() private returns (uint256) {
        tokenCounter.increment();
        return tokenCounter.current();
    }

    function getBaseURI() external view returns (string memory) {
        return baseURI;
    }

    function setBaseURI(string memory _baseURI) external onlyOwner {
        baseURI = _baseURI;
    }

    function totalMinted() external onlyOwner view returns(uint256) {
        return _totalMinted();
    }

    function numberMinted(address owner) external onlyOwner view returns(uint256) {
        return _numberMinted(owner);
    }
}